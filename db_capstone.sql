/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `comments` (
  `comment_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `image_id` int DEFAULT NULL,
  `date_comment` datetime DEFAULT NULL,
  `content` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `user_id` (`user_id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `image` (
  `image_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`image_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `save_img` (
  `user_id` int NOT NULL,
  `image_id` int NOT NULL,
  `date_save` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`image_id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `save_img_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `save_img_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `full_name` varchar(150) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `avatar` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `comments` (`comment_id`, `user_id`, `image_id`, `date_comment`, `content`) VALUES
(1, 1, 1, '2023-07-09 10:15:00', 'This is a great image!');
INSERT INTO `comments` (`comment_id`, `user_id`, `image_id`, `date_comment`, `content`) VALUES
(2, 2, 1, '2023-07-09 11:30:00', 'Beautifully captured!');
INSERT INTO `comments` (`comment_id`, `user_id`, `image_id`, `date_comment`, `content`) VALUES
(3, 3, 2, '2023-07-09 12:45:00', 'Love the colors in this picture.');
INSERT INTO `comments` (`comment_id`, `user_id`, `image_id`, `date_comment`, `content`) VALUES
(4, 4, 3, '2023-07-09 14:00:00', 'Amazing composition!'),
(5, 5, 4, '2023-07-09 15:15:00', 'The lighting is perfect!'),
(6, 1, 5, '2023-07-09 16:30:00', 'Incredible shot!'),
(7, 2, 6, '2023-07-09 17:45:00', 'This image tells a story.'),
(8, 3, 7, '2023-07-09 19:00:00', 'Great depth of field!'),
(9, 4, 8, '2023-07-09 20:15:00', 'I am impressed by the details.'),
(10, 5, 9, '2023-07-09 21:30:00', 'Well done, capturing the moment!'),
(11, 1, 10, '2023-07-09 22:45:00', 'This image is stunning!'),
(12, 2, 5, '2023-07-09 23:00:00', 'I feel inspired by this photo.'),
(13, 3, 3, '2023-07-09 23:15:00', 'Beautiful composition and colors.'),
(14, 4, 1, '2023-07-09 23:30:00', 'A picture worth a thousand words!'),
(15, 5, 7, '2023-07-09 23:45:00', 'Great job capturing the mood.');

INSERT INTO `image` (`image_id`, `name`, `url`, `description`, `user_id`) VALUES
(1, 'Image 1', 'https://example.com/image1.jpg', 'This is the first image', 1);
INSERT INTO `image` (`image_id`, `name`, `url`, `description`, `user_id`) VALUES
(2, 'Image 2', 'https://example.com/image2.jpg', 'This is the second image', 1);
INSERT INTO `image` (`image_id`, `name`, `url`, `description`, `user_id`) VALUES
(3, 'Image 3', 'https://example.com/image3.jpg', 'This is the third image', 2);
INSERT INTO `image` (`image_id`, `name`, `url`, `description`, `user_id`) VALUES
(4, 'Image 4', 'https://example.com/image4.jpg', 'This is the fourth image', 3),
(5, 'Image 5', 'https://example.com/image5.jpg', 'This is the fifth image', 4),
(6, 'Image 6', 'https://example.com/image6.jpg', 'This is the sixth image', 4),
(7, 'Image 7', 'https://example.com/image7.jpg', 'This is the seventh image', 5),
(8, 'Image 8', 'https://example.com/image8.jpg', 'This is the eighth image', 6),
(9, 'Image 9', 'https://example.com/image9.jpg', 'This is the ninth image', 6),
(10, 'Image 10', 'https://example.com/image10.jpg', 'This is the tenth image', 7);

INSERT INTO `save_img` (`user_id`, `image_id`, `date_save`) VALUES
(1, 1, '2023-07-09 10:15:00');
INSERT INTO `save_img` (`user_id`, `image_id`, `date_save`) VALUES
(1, 2, '2023-07-09 11:30:00');
INSERT INTO `save_img` (`user_id`, `image_id`, `date_save`) VALUES
(1, 3, '2023-07-09 12:45:00');
INSERT INTO `save_img` (`user_id`, `image_id`, `date_save`) VALUES
(2, 4, '2023-07-09 14:00:00'),
(2, 5, '2023-07-09 15:15:00'),
(2, 6, '2023-07-09 16:30:00'),
(3, 7, '2023-07-09 17:45:00'),
(3, 8, '2023-07-09 19:00:00'),
(3, 9, '2023-07-09 20:15:00'),
(4, 1, '2023-07-09 22:45:00'),
(4, 2, '2023-07-09 23:00:00'),
(4, 10, '2023-07-09 21:30:00'),
(5, 3, '2023-07-09 23:15:00'),
(5, 4, '2023-07-09 23:30:00'),
(5, 5, '2023-07-09 23:45:00');

INSERT INTO `user` (`user_id`, `email`, `password`, `full_name`, `age`, `avatar`) VALUES
(1, 'john.doe@mail.com', 'johndoe', 'John Doe', 25, 'avatar1.jpg');
INSERT INTO `user` (`user_id`, `email`, `password`, `full_name`, `age`, `avatar`) VALUES
(2, 'jane.smith@mail.com', 'janesmith', 'Jane Smith', 32, 'avatar2.jpg');
INSERT INTO `user` (`user_id`, `email`, `password`, `full_name`, `age`, `avatar`) VALUES
(3, 'david.johnson@mail.com', 'davidjohnson', 'David Johnson', 19, 'avatar3.jpg');
INSERT INTO `user` (`user_id`, `email`, `password`, `full_name`, `age`, `avatar`) VALUES
(4, 'emily.davis@mail.com', 'emilydavis', 'Emily Davis', 28, 'avatar4.jpg'),
(5, 'michael.brown@mail.com', 'michaelbrown', 'Michael Brown', 36, 'avatar5.jpg'),
(6, 'sarah.wilson@mail.com', 'sarahwilson', 'Sarah Wilson', 22, 'avatar6.jpg'),
(7, 'brian.taylor@mail.com', 'briantaylor', 'Brian Taylor', 31, 'avatar7.jpg'),
(8, 'lisa.anderson@mail.com', 'lisaanderson', 'Lisa Anderson', 29, 'avatar8.jpg'),
(9, 'daniel.lee@mail.com', 'daniellee', 'Daniel Lee', 24, 'avatar9.jpg'),
(10, 'michelle.thompson@mail.com', 'michellethompson', 'Michelle Thompson', 27, 'avatar10.jpg'),
(11, 'robert.miller@mail.com', 'robertmiller', 'Robert Miller', 33, 'avatar11.jpg'),
(12, 'amanda.martinez@mail.com', 'amandamartinez', 'Amanda Martinez', 26, 'avatar12.jpg'),
(13, 'matthew.clark@mail.com', 'matthewclark', 'Matthew Clark', 35, 'avatar13.jpg'),
(14, 'jennifer.rodriguez@mail.com', 'jenniferrodriguez', 'Jennifer Rodriguez', 30, 'avatar14.jpg'),
(15, 'william.harris@mail.com', 'williamharris', 'William Harris', 21, 'avatar15.jpg'),
(16, 'olivia.adams@mail.com', 'oliviaadams', 'Olivia Adams', 23, 'avatar16.jpg'),
(17, 'christopher.turner@mail.com', 'christopherturner', 'Christopher Turner', 34, 'avatar17.jpg'),
(18, 'sophia.wright@mail.com', 'sophiawright', 'Sophia Wright', 29, 'avatar18.jpg'),
(19, 'james.lopez@mail.com', 'jameslopez', 'James Lopez', 37, 'avatar19.jpg'),
(20, 'amy.johnson@mail.com', 'amyjohnson', 'Amy Johnson', 27, 'avatar20.jpg');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;