
import { decodeToken } from "../configs/jwt.js";
export const getUserIdFromToken = (req) => {
    const { token } = req.headers;
    const decodedData = decodeToken(token);
    return decodedData && decodedData.data && decodedData.data.user_id;
  };