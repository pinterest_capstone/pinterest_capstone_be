
import cloudinary from 'cloudinary';

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRECT,
  secure: true
});

export const uploadImageToCloudinary = (buffer, public_id) => {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader.upload_stream({
      resource_type: "image",
      public_id: public_id
    }, (error, result) => {
      if (error) reject(error);
      else resolve(result);
    }).end(buffer);
  });
};
