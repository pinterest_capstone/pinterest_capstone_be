import { PrismaClient } from "@prisma/client";
import { successCode, error, failCode } from "../configs/response.js";
import { getUserIdFromToken } from "../utils/getUserIdFromToken.js";
const prisma = new PrismaClient();
const getDetailImage = async (req, res) => {
  const image_id = req.params.image_id;
  const parsedImageId = parseInt(image_id);
  try {
    let image = await prisma.image.findUnique({
      where: {
        image_id: parsedImageId,
      },
      include: {
        comments: true, // This includes the comments related to the image
      },
    });

    if (image) {
      successCode(res, image, "Successfully showed detail image and comments");
    } else {
      failCode(res, "", "Image not found");
    } 
  } catch (err) {
    error(res, "Backend failure");   
  }
};

const addComments = async (req, res) => {
  const { image_id, content } = req.body;
  const user_id= getUserIdFromToken(req)
  const parsedImageId = parseInt(image_id);
  const date_comment = new Date().toISOString();
  let newData = {
    user: { connect: { user_id: user_id} },
    image: { connect: { image_id: parsedImageId } },
    content,
    date_comment,
  };
  if(newData){

    try {
      
      let createComment = await prisma.comments.create({ data: newData });
      if (createComment) {
        successCode(res, "", "Successfully added comment");
      } 
   
    } catch (error) {
     
        failCode(res, "", "Image not found");
      
    }
  }else{
    error(res,"BE fail")
  }
};
const showComments = async (req, res) => {
  try {
    const image_id = parseInt(req.params.image_id);
    const data = await prisma.image.findUnique({
      where: {
        image_id,
      },
      include: {
        comments: true,
      },
    });

    if (data) {
      successCode(res, { image: data, comments: data.comments }, "Showed All Comments related to specific img");
    } else {
      failCode(res, "", "No image found with the given ID");
    }
  } catch (err) {
    error(res, "BE fail");
  }
};

const saveImg = async (req, res) => {
  try {
    const { image_id} = req.body;
    const user_id =getUserIdFromToken(req)
    const parsedImageId = parseInt(image_id);
    const date_save = new Date().toISOString();
    let data = await prisma.save_img.findFirst({
      where: {
        image_id: parsedImageId,
        user_id,
      },
    });

    if (data) {
      await prisma.save_img.delete({
        where: {
          user_id_image_id: {
            user_id,
            image_id: parsedImageId,
          },
        },
      });
      successCode(res, "", "Sucessfully unsaved image");
    } else {
      const addData = {
        user_id,
        image_id: parsedImageId,
        date_save,
      };
      await prisma.save_img.create({ data: addData });
      successCode(res, "", "Successfully save image");
    }
  } catch (err) {
    failCode(res, "", "Image not found");
  }
};
export { getDetailImage, addComments, showComments, saveImg };
