import { failCode, successCode, error } from "../configs/response.js";
import bcrypt from "bcrypt";
import { PrismaClient } from "@prisma/client";
import { decodeToken, generateToken } from "../configs/jwt.js";
import { uploadImageToCloudinary } from "../utils/cloudinaryUtils.js";
const prisma = new PrismaClient();
import { getUserIdFromToken } from "../utils/getUserIdFromToken.js";
const signUp = async (req, res) => {
  try {
    let { email, password, full_name, age } = req.body;
    let checkUser = await prisma.user.findFirst({ where: { email } });
    if (checkUser) {
      failCode(res, "", "Email already exists");
    } else {
      let newUser = {
        email,
        password: bcrypt.hashSync(password, 10),
        full_name,
        age,
      };
      await prisma.user.create({ data: newUser });
      successCode(res, "", "Create account successfully");
    }
  } catch {
    error(res, "Server Disconect");
  }
};

const signIn = async (req, res) => {
  try {
    let { email, password } = req.body;
    let checkUser = await prisma.user.findFirst({ where: { email } });
    if (checkUser) {
      if (bcrypt.compareSync(password, checkUser.password)) {
        checkUser = { ...checkUser, password: "" };

        let token = generateToken(checkUser);
        successCode(
          res,
          { user_id: checkUser.user_id, token: token },
          "Login successfully"
        );
      } else {
        failCode(res, "", "Email or password incorrect !");
      }
    } else {
      failCode(res, "", "Email or password incorrect !");
    }
  } catch (error) {
    failCode(res, "", error.message);
  }
};

const getUserByToken = async (req, res) => {
  try {
    let { token } = req.headers;

    const decodedToken = decodeToken(token);
    const user_id = decodedToken.data.user_id;

    const user = await prisma.user.findFirst({ where: { user_id } });

    if (user) {
      const userWithoutPassWord = { ...user, password: "" };
      successCode(res, userWithoutPassWord, "Login successfully");
    } else {
      failCode(res, "", "User not found");
    }
  } catch (error) {
    failCode(res, "", error.message);
  }
};

const updateAvatar = async (req, res) => {
  try {
    const user_id = getUserIdFromToken(req);
    const checkUser = await prisma.user.findFirst({
      where: { user_id },
    });
    if (checkUser) {
      if (req.file) {
        const cloudinaryResult = await uploadImageToCloudinary(
          req.file.buffer,
          `capstone_pinterest/user_${user_id}_avatar`
        );

        // capture the updated user data
        const updatedUser = await prisma.user.update({
          where: { user_id },
          data: { avatar: cloudinaryResult.secure_url },
          // To fetch back all fields of the updated record, include this
          select: {
            user_id: true,
            avatar: true,
            // include other fields you need
          },
        });

        // return the updated user data in the response
        successCode(res, updatedUser, "Avatar updated successfully");
      } else {
        failCode(res, "", "No file uploaded");
      }
    } else {
      failCode(res, "", "User not found");
    }
  } catch (error) {
    failCode(res, "", error.message);
  }
};

const updateUserInfo = async (req, res) => {
  try {
    let { user_id, email, password, full_name, age } = req.body;
    let updates = { user_id, email, password, full_name, age };
    if (updates.password) {
      const hashedPassword = await bcrypt.hash(updates.password, 10);
      updates.password = hashedPassword;
    }
    const checkUser = await prisma.user.findUnique({
      where: { user_id },
    });
    if (checkUser) {
      let updateUser = { ...checkUser, ...updates };
      await prisma.user.update({ where: { user_id }, data: updateUser });
      successCode(res, updateUser, "User info updated successfully");
    }
  } catch (err) {
    error(res, err.message);
  }
};

export { signUp, signIn, getUserByToken, updateAvatar, updateUserInfo };
