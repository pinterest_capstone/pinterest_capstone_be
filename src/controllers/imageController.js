import { PrismaClient } from "@prisma/client";
import { successCode, error, failCode } from "../configs/response.js";
import { v2 as cloudinary } from "cloudinary";
import { config } from "dotenv";
import { getUserIdFromToken } from "../utils/getUserIdFromToken.js";
import { uploadImageToCloudinary } from "../utils/cloudinaryUtils.js";
import { json } from "express";

const prisma = new PrismaClient();

const getUserDetail = async (req, res) => {
  const user_id = getUserIdFromToken(req);
  try {
    let data = await prisma.user.findFirst({
      where: {
        user_id,
      },
    });
    if (data) {
      successCode(res, data, "User ID loaded");
    } else {
      failCode(res, "", " User Not found");
    }
  } catch (err) {
    error(res, "BE fail");
  }
};

const getImageListSaveByUser = async (req, res) => {
  const user_id = getUserIdFromToken(req);
  try {
    let data = await prisma.save_img.findMany({
      where: {
        user_id,
      },
      include: {
        image: true,
      },
    });

    if (data.length > 0) {
      data = data.map((item) => {
        const { image, ...rest } = item;
        return {
          ...rest,
          image: {
            name: image.name,
            url: image.url,
            description: image.description,
          },
        };
      });
      successCode(res, data, "Saved Image List Information Loaded");
    } else {
      failCode(res, "", "User hasn't save any img");
    }
  } catch (err) {
    error(res, "BE fail");
  }
};

const getImageListCreatedByUser = async (req, res) => {
  const user_id = getUserIdFromToken(req);
  try {
    let data = await prisma.image.findMany({
      where: {
        user_id,
      },
    });

    if (data.length > 0) {
      successCode(res, data, "Created Image List Information Loaded");
    } else {
      failCode(res, "", "User hasn't created any img");
    }
  } catch (err) {
    error(res, "BE fail");
  }
};

const deleteImg = async (req, res) => {
  const user_id = getUserIdFromToken(req);
  const image_id = parseInt(req.body.image_id);

  try {
    let imageFound = await prisma.image.findFirst({
      where: {
        image_id,
      },
    });

    if (!imageFound) {
      failCode(res, "", "Image not found");
      return;
    }

    if (imageFound.user_id !== user_id) {
      failCode(res, "", "You don't have permission to delete this image");
      return;
    }

    await prisma.save_img.deleteMany({
      where: {
        image_id: imageFound.image_id,
      },
    });
    await prisma.comments.deleteMany({
      where: {
        image_id: imageFound.image_id,
      },
    });
    await prisma.image.delete({
      where: {
        image_id: imageFound.image_id,
      },
    });

    successCode(res, imageFound, "Image Deleted Successfully");
  } catch (err) {
    error(res, "BE fail");
  }
};

const addImage = async (req, res) => {
  const { name, description } = req.body;
  const user_id_int = getUserIdFromToken(req);

  if (!req.file) {
    error(res, "No image file was provided");
    return;
  }

  try {
    const cloudinaryResult = await uploadImageToCloudinary(
      req.file.buffer,
      `capstone_pinterest/${name}`
    );

    const result = await prisma.image.create({
      data: {
        name,
        url: cloudinaryResult.secure_url,
        description,
        user_id: user_id_int,
      },
    });

    successCode(res, result, "Image Uploaded Successfully");
  } catch (err) {
    error(res, "Image upload failed");
  }
};

const getImage = async (req, res) => {
  let dataImg = await prisma.image.findMany();
  if (dataImg.length > 0) {
    successCode(res, dataImg, "Images loaded");
  } else {
    failCode(res, "", "Database disconect");
  }
};

const getImageByName = async (req, res) => {
  let { searchName } = req.body;
  if (!searchName) {
    failCode(res, "", "Missing search name parameter");
  }
  try {
    const images = await prisma.image.findMany({
      where: {
        name: {
          contains: searchName,
        },
      },
    });
    successCode(res, images, "Founded image successfully");
  } catch (err) {
    error(res, err.message);
  }
};

export {
  getUserDetail,
  getImageListSaveByUser,
  getImageListCreatedByUser,
  deleteImg,
  addImage,
  getImage,
  getImageByName,
};
