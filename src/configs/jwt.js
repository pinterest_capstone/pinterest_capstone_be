import jwt from "jsonwebtoken";
import config from "./config.js";
import { failCode } from "./response.js";

const { secrect_key } = config;
const SECRECT_KEY = secrect_key.toString();

const generateToken = (data) => {
  return jwt.sign({ data }, SECRECT_KEY, {
    expiresIn: "24h",
    algorithm: "HS256",
  });
};

const checkToken = (token) => {
  return jwt.verify(token, SECRECT_KEY);
};

const decodeToken = (token) => {
  return jwt.decode(token);
};

const tokenApi = (req, res, next) => {
  try {
    let { token } = req.headers;
    if (!token) {
      failCode(res, "", "Unauthorized");
    } else {
      if (checkToken(token)) {
        next();
      }
    }
  } catch (error) {
    failCode(res, "", error.message);
  }
};

export { tokenApi, generateToken, checkToken, decodeToken };
