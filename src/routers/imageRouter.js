import express from "express";
import {
  addComments,
  getDetailImage,
  saveImg,
  showComments,
} from "../controllers/detailPage.js";
import {
  addImage,
  getImage,
  getImageByName,
} from "../controllers/imageController.js";
import { tokenApi } from "../configs/jwt.js";
import upload from "../controllers/upload.js";

const imageRouter = express.Router();

imageRouter.get("/get-detail-img/:image_id", tokenApi, getDetailImage);
imageRouter.post("/post-comment", tokenApi, addComments);
imageRouter.get("/get-comments-for-one-img/:image_id", tokenApi, showComments);
imageRouter.post("/save-img", tokenApi, saveImg);
imageRouter.post("/upload", tokenApi, upload.single("image"), addImage);
imageRouter.get("/load-img", getImage);
imageRouter.get("/load-img-by-name", getImageByName);

export default imageRouter;
