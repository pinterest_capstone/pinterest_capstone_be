import express from "express";
import {
  getUserByToken,
  signIn,
  signUp,
  updateAvatar,
  updateUserInfo,
} from "../controllers/userController.js";
import {
  deleteImg,
  getImageListCreatedByUser,
  getImageListSaveByUser,
  getUserDetail,
} from "../controllers/imageController.js";
import { tokenApi } from "../configs/jwt.js";
import upload from "../controllers/upload.js";

const userRouter = express.Router();

userRouter.post("/signin", signIn);
userRouter.post("/signup", signUp);
userRouter.get("/get-user-by-token", tokenApi, getUserByToken);
userRouter.put(
  "/update-user-avatar",
  tokenApi,
  upload.single("image"),
  updateAvatar
);
userRouter.put("/update-user-info", tokenApi, updateUserInfo);
//
userRouter.get("/get-detail-user", tokenApi, getUserDetail);
userRouter.get("/get-list-save-img", tokenApi, getImageListSaveByUser);
userRouter.get("/get-list-create-img", tokenApi, getImageListCreatedByUser);
userRouter.delete("/delete-img", tokenApi, deleteImg);
export default userRouter;
